# 微信服务号模板消息怎么用？能给所有粉丝群发吗？

#### Description
实现微信服务号给所有粉丝群发模板消息，微号帮提供了模板消息群发功能实现，可以帮助服务号在线群发模板消息，可以给所有粉丝或分组粉丝群发，可以设置免打扰分组，即该分组粉丝不接受群发的模板消息，可以定时未来30天内的群发任务等...

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
